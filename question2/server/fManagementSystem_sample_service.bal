import ballerina/grpc;

listener grpc:Listener ep = new (9090);

functionVersionList listF = {};



@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "fManagementSystem" on ep {

    remote function add_new_fn(functionVersionList value) returns confirmationFunction|error {

confirmationFunction re = {confirmation:true};

        listF.push(value);


        var x = ep->send(re);
        if(x is error) {
            io:println("error");
        }else {
            io:println("not error ");
        }
    }
    remote function add_fns(functionCollection value) returns confirmationFunction|error {
    }
    remote function delete_fn(functionKey value) returns confirmationFunction|error {
    }
    remote function show_fn(functionKey value) returns metaData|error {
    }
    remote function show_all_fns(functionKey value) returns functionVersionList|error {
    }
    remote function show_all_with_criteria(functionSearch value) returns functionCollection|error {
    }
}

