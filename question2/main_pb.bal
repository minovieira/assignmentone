import ballerina/grpc;

public isolated client class fManagementSystemClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(functionVersionList|ContextFunctionVersionList req) returns (confirmationFunction|grpc:Error) {
        map<string|string[]> headers = {};
        functionVersionList message;
        if (req is ContextFunctionVersionList) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <confirmationFunction>result;
    }

    isolated remote function add_new_fnContext(functionVersionList|ContextFunctionVersionList req) returns (ContextConfirmationFunction|grpc:Error) {
        map<string|string[]> headers = {};
        functionVersionList message;
        if (req is ContextFunctionVersionList) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <confirmationFunction>result, headers: respHeaders};
    }

    isolated remote function add_fns(functionCollection|ContextFunctionCollection req) returns (confirmationFunction|grpc:Error) {
        map<string|string[]> headers = {};
        functionCollection message;
        if (req is ContextFunctionCollection) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <confirmationFunction>result;
    }

    isolated remote function add_fnsContext(functionCollection|ContextFunctionCollection req) returns (ContextConfirmationFunction|grpc:Error) {
        map<string|string[]> headers = {};
        functionCollection message;
        if (req is ContextFunctionCollection) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <confirmationFunction>result, headers: respHeaders};
    }

    isolated remote function delete_fn(functionKey|ContextFunctionKey req) returns (confirmationFunction|grpc:Error) {
        map<string|string[]> headers = {};
        functionKey message;
        if (req is ContextFunctionKey) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <confirmationFunction>result;
    }

    isolated remote function delete_fnContext(functionKey|ContextFunctionKey req) returns (ContextConfirmationFunction|grpc:Error) {
        map<string|string[]> headers = {};
        functionKey message;
        if (req is ContextFunctionKey) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <confirmationFunction>result, headers: respHeaders};
    }

    isolated remote function show_fn(functionKey|ContextFunctionKey req) returns (metaData|grpc:Error) {
        map<string|string[]> headers = {};
        functionKey message;
        if (req is ContextFunctionKey) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <metaData>result;
    }

    isolated remote function show_fnContext(functionKey|ContextFunctionKey req) returns (ContextMetaData|grpc:Error) {
        map<string|string[]> headers = {};
        functionKey message;
        if (req is ContextFunctionKey) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <metaData>result, headers: respHeaders};
    }

    isolated remote function show_all_fns(functionKey|ContextFunctionKey req) returns (functionVersionList|grpc:Error) {
        map<string|string[]> headers = {};
        functionKey message;
        if (req is ContextFunctionKey) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <functionVersionList>result;
    }

    isolated remote function show_all_fnsContext(functionKey|ContextFunctionKey req) returns (ContextFunctionVersionList|grpc:Error) {
        map<string|string[]> headers = {};
        functionKey message;
        if (req is ContextFunctionKey) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <functionVersionList>result, headers: respHeaders};
    }

    isolated remote function show_all_with_criteria(functionSearch|ContextFunctionSearch req) returns (functionCollection|grpc:Error) {
        map<string|string[]> headers = {};
        functionSearch message;
        if (req is ContextFunctionSearch) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <functionCollection>result;
    }

    isolated remote function show_all_with_criteriaContext(functionSearch|ContextFunctionSearch req) returns (ContextFunctionCollection|grpc:Error) {
        map<string|string[]> headers = {};
        functionSearch message;
        if (req is ContextFunctionSearch) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("service.fManagementSystem/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <functionCollection>result, headers: respHeaders};
    }
}

public client class FManagementSystemConfirmationFunctionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendConfirmationFunction(confirmationFunction response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextConfirmationFunction(ContextConfirmationFunction response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FManagementSystemFunctionCollectionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunctionCollection(functionCollection response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunctionCollection(ContextFunctionCollection response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FManagementSystemMetaDataCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendMetaData(metaData response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextMetaData(ContextMetaData response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FManagementSystemFunctionVersionListCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunctionVersionList(functionVersionList response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunctionVersionList(ContextFunctionVersionList response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextMetaData record {|
    metaData content;
    map<string|string[]> headers;
|};

public type ContextFunctionVersionList record {|
    functionVersionList content;
    map<string|string[]> headers;
|};

public type ContextConfirmationFunction record {|
    confirmationFunction content;
    map<string|string[]> headers;
|};

public type ContextFunctionCollection record {|
    functionCollection content;
    map<string|string[]> headers;
|};

public type ContextFunctionKey record {|
    functionKey content;
    map<string|string[]> headers;
|};

public type ContextFunctionSearch record {|
    functionSearch content;
    map<string|string[]> headers;
|};

public type metaData record {|
    string Id = "";
    string fullName = "";
    string email = "";
    string languageUsed = "";
    boolean isNew = false;
    int 'Version = 0;
    functionTags functionTags = {};
|};

public type functionTags record {|
    string[] tag = [];
|};

public type functionVersionList record {|
    metaData[] functions = [];
|};

public type confirmationFunction record {|
    boolean confirmation = false;
|};

public type functionKey record {|
    string Id = "";
    string ver = "";
|};

public type functionCollection record {|
    functionVersionList[] functionVersionsList = [];
|};

public type functionSearch record {|
    string searchtype = "";
    functionTags tags = {};
    string LangUsed = "";
|};

const string ROOT_DESCRIPTOR = "0A0A6D61696E2E70726F746F1207736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22DB010A086D65746144617461120E0A02496418012001280952024964121A0A0866756C6C4E616D65180220012809520866756C6C4E616D6512140A05656D61696C1803200128095205656D61696C12220A0C6C616E677561676555736564180420012809520C6C616E67756167655573656412140A0569734E6577180520012808520569734E657712180A0756657273696F6E180620012805520756657273696F6E12390A0C66756E6374696F6E5461677318072001280B32152E736572766963652E66756E6374696F6E54616773520C66756E6374696F6E5461677322200A0C66756E6374696F6E5461677312100A037461671801200328095203746167222F0A0B66756E6374696F6E4B6579120E0A0249641801200128095202496412100A03766572180220012809520376657222460A1366756E6374696F6E56657273696F6E4C697374122F0A0966756E6374696F6E7318012003280B32112E736572766963652E6D65746144617461520966756E6374696F6E7322770A0E66756E6374696F6E536561726368121E0A0A73656172636874797065180120012809520A7365617263687479706512290A047461677318022001280B32152E736572766963652E66756E6374696F6E54616773520474616773121A0A084C616E675573656418032001280952084C616E675573656422660A1266756E6374696F6E436F6C6C656374696F6E12500A1466756E6374696F6E56657273696F6E734C69737418012003280B321C2E736572766963652E66756E6374696F6E56657273696F6E4C697374521466756E6374696F6E56657273696F6E734C697374223A0A14636F6E6669726D6174696F6E46756E6374696F6E12220A0C636F6E6669726D6174696F6E180120012808520C636F6E6669726D6174696F6E32AF030A11664D616E6167656D656E7453797374656D12490A0A6164645F6E65775F666E121C2E736572766963652E66756E6374696F6E56657273696F6E4C6973741A1D2E736572766963652E636F6E6669726D6174696F6E46756E6374696F6E12450A076164645F666E73121B2E736572766963652E66756E6374696F6E436F6C6C656374696F6E1A1D2E736572766963652E636F6E6669726D6174696F6E46756E6374696F6E12400A0964656C6574655F666E12142E736572766963652E66756E6374696F6E4B65791A1D2E736572766963652E636F6E6669726D6174696F6E46756E6374696F6E12320A0773686F775F666E12142E736572766963652E66756E6374696F6E4B65791A112E736572766963652E6D6574614461746112420A0C73686F775F616C6C5F666E7312142E736572766963652E66756E6374696F6E4B65791A1C2E736572766963652E66756E6374696F6E56657273696F6E4C697374124E0A1673686F775F616C6C5F776974685F637269746572696112172E736572766963652E66756E6374696F6E5365617263681A1B2E736572766963652E66756E6374696F6E436F6C6C656374696F6E620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "main.proto": "0A0A6D61696E2E70726F746F1207736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22DB010A086D65746144617461120E0A02496418012001280952024964121A0A0866756C6C4E616D65180220012809520866756C6C4E616D6512140A05656D61696C1803200128095205656D61696C12220A0C6C616E677561676555736564180420012809520C6C616E67756167655573656412140A0569734E6577180520012808520569734E657712180A0756657273696F6E180620012805520756657273696F6E12390A0C66756E6374696F6E5461677318072001280B32152E736572766963652E66756E6374696F6E54616773520C66756E6374696F6E5461677322200A0C66756E6374696F6E5461677312100A037461671801200328095203746167222F0A0B66756E6374696F6E4B6579120E0A0249641801200128095202496412100A03766572180220012809520376657222460A1366756E6374696F6E56657273696F6E4C697374122F0A0966756E6374696F6E7318012003280B32112E736572766963652E6D65746144617461520966756E6374696F6E7322770A0E66756E6374696F6E536561726368121E0A0A73656172636874797065180120012809520A7365617263687479706512290A047461677318022001280B32152E736572766963652E66756E6374696F6E54616773520474616773121A0A084C616E675573656418032001280952084C616E675573656422660A1266756E6374696F6E436F6C6C656374696F6E12500A1466756E6374696F6E56657273696F6E734C69737418012003280B321C2E736572766963652E66756E6374696F6E56657273696F6E4C697374521466756E6374696F6E56657273696F6E734C697374223A0A14636F6E6669726D6174696F6E46756E6374696F6E12220A0C636F6E6669726D6174696F6E180120012808520C636F6E6669726D6174696F6E32AF030A11664D616E6167656D656E7453797374656D12490A0A6164645F6E65775F666E121C2E736572766963652E66756E6374696F6E56657273696F6E4C6973741A1D2E736572766963652E636F6E6669726D6174696F6E46756E6374696F6E12450A076164645F666E73121B2E736572766963652E66756E6374696F6E436F6C6C656374696F6E1A1D2E736572766963652E636F6E6669726D6174696F6E46756E6374696F6E12400A0964656C6574655F666E12142E736572766963652E66756E6374696F6E4B65791A1D2E736572766963652E636F6E6669726D6174696F6E46756E6374696F6E12320A0773686F775F666E12142E736572766963652E66756E6374696F6E4B65791A112E736572766963652E6D6574614461746112420A0C73686F775F616C6C5F666E7312142E736572766963652E66756E6374696F6E4B65791A1C2E736572766963652E66756E6374696F6E56657273696F6E4C697374124E0A1673686F775F616C6C5F776974685F637269746572696112172E736572766963652E66756E6374696F6E5365617263681A1B2E736572766963652E66756E6374696F6E436F6C6C656374696F6E620670726F746F33"};
}

